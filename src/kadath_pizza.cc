#include <cctk.h>
#include <cctk_Parameters.h>
#include <cctk_Arguments.h>
#include "pizzacactus.h"
#include "pizza_idbase.h"
#include <string>
#include <chrono>
#include <stdexcept>
extern "C" void Kadath_Pizza_Startup(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("Setting up coordinates");

  int const npoints = cctk_lsh[0] * cctk_lsh[1] * cctk_lsh[2];
  std::vector<double> xx(npoints), yy(npoints), zz(npoints);

  #pragma omp parallel for
  for (int i = 0; i < npoints; ++i) {
    xx[i] = x[i];
    yy[i] = y[i];
    zz[i] = z[i];
  }

  auto start = std::chrono::system_clock::now();

  EOS_Barotropic::eos_1p eos  = Pizza::IDBase::pizza_idbase_central::get().eos;
  #pragma omp parallel for
  for (int i = 0; i < npoints; ++i) {
    try {  
      const double gm1 = eos.gm1_from_rmd(rho[i]);
      temperature[i]  = eos.temp_from_gm1(gm1);
      Y_e[i] = eos.efrac_from_gm1(gm1);
    }
    catch (...) {
      CCTK_VInfo(CCTK_THORNSTRING, "Bad rho encountered = %f", rho[i]);
    }
  } // for i

  auto end = std::chrono::system_clock::now();

  std::chrono::duration<double> elapsed_seconds = end - start;

  CCTK_VInfo(CCTK_THORNSTRING, "Filling took %g sec", elapsed_seconds.count());

  
}
